$dllLocation = Join-Path $PSScriptRoot '..\..\closedSource\HDFql.dll';
$soLocation = Join-Path $PSScriptRoot '..\..\closedSource\HDFql.so';

$dllDestination = Join-Path $PSScriptRoot '.\bin\Debug\netcoreapp2.0\HDFql.dll';
$soDestination = Join-Path $PSScriptRoot '.\bin\Debug\netcoreapp2.0\HDFql.so';

if (Test-Path -Path (Split-Path -Path $dllLocation -Parent)) {} else {mkdir (Test-Path -Path (Split-Path -Path $dllLocation -Parent))};

Copy-Item -Path $dllLocation -Destination $dllDestination;
Copy-Item -Path $soLocation -Destination $soDestination;


